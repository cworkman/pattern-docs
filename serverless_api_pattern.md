# Serverless API Patterns
Serverless API designs have to embress asyncronous design patterns to best utilize FaaS resources, which are heavily modeled around execution time. These patterns deviate from some conventional thinking and wisdom from traditional infrastructure in important ways, and these document will summarize some of the common tropes visually.

## Async Processing
Requests from cliens to your serverless API cannot expect the side affects of their request to occur within the duration of their request. Instead of thinking in terms of 'I need to do this thing', think 'I need this thing to get started'. 

```mermaid
sequenceDiagram
    participant client
    participant api
    participant db
    participant queue
    participant worker

    client ->>+ api: idempotent PUT { ...data }
    api ->> db: create job IN_PROGRESS
    note left of db: Assume the job is<br>IN_PROGRESS because<br>side effects WILL occur
    api ->> queue: insert event for processing
    api -->>- client: OK

    queue ->> worker: invoke function from event source
    note left of worker: The worker completes its task
    worker ->>+ api: callback result
    api ->> db: job COMPLETED
    api -->>- worker: OK
    
```

## Optimistic Locking
This is the internet, where things go wrong. Your requests may come out of order, multiple times, or no times at all. It is our responsibility to make sure we prevent unwanted side effects such as data loss, stale database writes, and all manner of race conditions in our application and even our data layer.

Notice in this sequence that the ETag values are validated in the database wite conditions. This is imperative, because multiple threads or instances could concurrently read and write the database at the same time.

```mermaid
sequenceDiagram
    participant client
    participant api
    participant db

    client ->>+ api: PUT {...data} w/o If-Match Header
    api ->>+ db: save if_not_exists(id)
    db -->>- api: success
    api -X client: OK, ETag: 1
    note left of api: response drops

    client ->>+ api: Retry PUT w/o If-Match Header
    api ->>+ db: save if_not_exists(id)
    db -->>- api: condition check failed
    note left of db: data already exists
    api -->>- client: 409 - Precondition Failed, current state, ETag: 1
    note right of client: client learns its state is stale

    client ->>+ api: PUT {...data} If-Match: 1
    api ->>+ db: save if_equals(etag:1)
    db -->>- api: success
    api -x- client: OK, ETag: 2

    client ->>+ api: Retry PUT If-Match: 1
    api ->>+ db: save if_equals(etag:1)
    db -->>- api: condition check failed
    api -->>- client: 409 - current state + ETag: 2

    client ->>+ api: Get If-None-Match: 2
    api ->>+ db: get data
    db -->- api: data, ETag: 2
    api -->- client: 304 No Modified
    note left of api: client is up to date<br>send empty body

```